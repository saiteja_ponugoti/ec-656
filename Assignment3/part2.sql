DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `switchSection`(
	IN courseID_in CHAR(8), 
    IN section1_in INT, 
    IN section2_in INT, 
    IN termCode_in DECIMAL(4), 
    IN quantity INT
    )
BEGIN

declare error_code INT default 5;
declare count_sec1 INT default -1;
declare count_sec2 INT default -1;
declare sec1_result INT default 1000;
declare sec2_result INT default -1;

	START TRANSACTION;
		SELECT COUNT(*) INTO count_sec1 FROM Offering o WHERE o.courseID = courseID_in and section = section1_in and termCode = termCode_in ;
		SELECT COUNT(*) INTO count_sec2 FROM Offering o WHERE o.courseID = courseID_in and section = section2_in and termCode = termCode_in ;
        SELECT enrollment - quantity INTO sec1_result FROM Offering o WHERE o.courseID = courseID_in and section = section1_in and termCode = termCode_in ;
		SELECT Capacity - enrollment INTO sec2_result FROM Offering o JOIN Classroom c ON c.roomID = o.roomID WHERE o.courseID = courseID_in and section = section2_in and termCode = termCode_in ;
        
        
        IF section1_in = section2_in OR count_sec1 = 0 OR count_sec2 = 0 OR quantity <= 0 THEN SET error_code = -1;
		ELSEIF sec1_result <0  AND error_code != -1 THEN SET error_code = -2;
        ELSEIF sec2_result <quantity AND error_code != -2 AND error_code != -1 THEN SET error_code = -3;
        ELSE SET error_code = 0;
        END IF;
        

        IF error_code = 0 THEN 
			BEGIN
				UPDATE Offering set enrollment = enrollment- quantity WHERE courseId = courseID_in and section = section1_in and termCode = termCode_in;
				UPDATE Offering set enrollment = enrollment+ quantity WHERE courseId = courseID_in and section = section2_in and termCode = termCode_in;
                SELECT error_code;
                COMMIT;
			END;
		ELSE
			BEGIN
				SELECT error_code;
				ROLLBACK ;
            END;
		
        END IF;
END$$
DELIMITER ;



-- error_code = -1 test cases 
-- a) if (courseID,section1,termCode) or (courseID,section2,termCode) do not exist in Offering
-- 'ECE359' is not in Offering
CALL `uni`.`switchSection`('ECE359',1, 2, 1191,18);
-- 'ECE356' there is no section 3 available
CALL `uni`.`switchSection`('ECE356',1, 3, 1191,18);
-- 'ECE356' is not offered when the term is 1192 
CALL `uni`.`switchSection`('ECE356',1, 2, 1192,18);

-- b) when quantity <=0
 CALL `uni`.`switchSection`('ECE356',1, 2, 1192,0);
  CALL `uni`.`switchSection`('ECE356',1, 2, 1192,-3);

-- c) when section1 = section2
CALL `uni`.`switchSection`('ECE356',2, 2, 1191,18);
CALL `uni`.`switchSection`('ECE356',1, 1, 1191,18);


-- error_code = -2 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,66);
CALL `uni`.`switchSection`('ECE356',2, 1, 1191,124);

-- error_code = -3 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,20);

-- error_code = 0 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,10);
CALL `uni`.`switchSection`('ECE356',2, 1, 1191,40);


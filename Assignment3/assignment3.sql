-- ================================================================================================================
-- PART 2 REQUIRED SQL 
-- ================================================================================================================

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `switchSection`(
	IN courseID_in CHAR(8), 
    IN section1_in INT, 
    IN section2_in INT, 
    IN termCode_in DECIMAL(4), 
    IN quantity INT
    )
BEGIN

declare error_code INT default 5;
declare count_sec1 INT default -1;
declare count_sec2 INT default -1;
declare sec1_result INT default 1000;
declare sec2_result INT default -1;

	START TRANSACTION;
		SELECT COUNT(*) INTO count_sec1 FROM Offering o WHERE o.courseID = courseID_in and section = section1_in and termCode = termCode_in ;
		SELECT COUNT(*) INTO count_sec2 FROM Offering o WHERE o.courseID = courseID_in and section = section2_in and termCode = termCode_in ;
        SELECT enrollment - quantity INTO sec1_result FROM Offering o WHERE o.courseID = courseID_in and section = section1_in and termCode = termCode_in ;
		SELECT Capacity - enrollment INTO sec2_result FROM Offering o JOIN Classroom c ON c.roomID = o.roomID WHERE o.courseID = courseID_in and section = section2_in and termCode = termCode_in ;
        
        
        IF section1_in = section2_in OR count_sec1 = 0 OR count_sec2 = 0 OR quantity <= 0 THEN SET error_code = -1;
		ELSEIF sec1_result <0  AND error_code != -1 THEN SET error_code = -2;
        ELSEIF sec2_result <quantity AND error_code != -2 AND error_code != -1 THEN SET error_code = -3;
        ELSE SET error_code = 0;
        END IF;
        

        IF error_code = 0 THEN 
			BEGIN
				UPDATE Offering set enrollment = enrollment- quantity WHERE courseId = courseID_in and section = section1_in and termCode = termCode_in;
				UPDATE Offering set enrollment = enrollment+ quantity WHERE courseId = courseID_in and section = section2_in and termCode = termCode_in;
                SELECT error_code;
                COMMIT;
			END;
		ELSE
			BEGIN
				SELECT error_code;
				ROLLBACK ;
            END;
		
        END IF;
END$$
DELIMITER ;


-- ========================================= TEST CASES ==================================================

-- error_code = -1 test cases 
-- a) if (courseID,section1,termCode) or (courseID,section2,termCode) do not exist in Offering
-- 'ECE359' is not in Offering
CALL `uni`.`switchSection`('ECE359',1, 2, 1191,18);
-- 'ECE356' there is no section 3 available
CALL `uni`.`switchSection`('ECE356',1, 3, 1191,18);
-- 'ECE356' is not offered when the term is 1192 
CALL `uni`.`switchSection`('ECE356',1, 2, 1192,18);

-- b) when quantity <=0
CALL `uni`.`switchSection`('ECE356',1, 2, 1192,0);
CALL `uni`.`switchSection`('ECE356',1, 2, 1192,-3);

-- c) when section1 = section2
CALL `uni`.`switchSection`('ECE356',2, 2, 1191,18);
CALL `uni`.`switchSection`('ECE356',1, 1, 1191,18);

-- error_code = -2 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,66);
CALL `uni`.`switchSection`('ECE356',2, 1, 1191,124);

-- error_code = -3 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,20);

-- error_code = 0 test cases
CALL `uni`.`switchSection`('ECE356',1, 2, 1191,10);
CALL `uni`.`switchSection`('ECE356',2, 1, 1191,40);


-- ================================================================================================================
-- PART 4 REQUIRED SQL 
-- ================================================================================================================
-- Question 1
select 
(select count(*) as white_wins from gameStatus  where winner = 'white')/
(select count(*) as total_game from gameStatus) as fraction;

-- Question 2
-- when white wins
select AVG(turns) from chessGames c join gameStatus g on c.id = g.id where g.winner = 'white'; 

-- when Black wins
select AVg(turns) from chessGames c
join gameStatus g on c.id = g.id
where g.winner = 'black'; 


-- Question 3

select sum(case when length(g.move)<3 then 1 else 0  end)/count(g.move) as fraction_games_started_pwan from gameMoves as g
join (select min(moveid) as moveid from gameMoves group by id ) as c on c.moveid=g.moveid;

-- Question 4

-- Average moves before moving Knights for White :
      
select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - s.mn)/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'N') as b ON a.id = b.id 
where LEFT(a.move, 1)= 'N' GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 0  group by f.id ORDER BY f.id ) n ;

-- Average moves before moving Bishops for White :

select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - s.mn)/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'B') as b ON a.id = b.id 
where LEFT(a.move, 1)= 'B' GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 0  group by f.id ORDER BY f.id ) n ;

-- Average moves before moving Rooks (Castles) for White :

select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - s.mn)/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'R' OR LEFT(move, 1)= 'O') as b ON a.id = b.id 
where LEFT(move, 1)= 'R' OR LEFT(move, 1)= 'O'  GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 0  group by f.id ORDER BY f.id ) n ;


-- Question 5

-- Average moves before moving Knights for Black :
select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - (s.mn+1))/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'N') as b ON a.id = b.id 
where LEFT(a.move, 1)= 'N' GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 1  group by f.id ORDER BY f.id ) n ;

-- Average moves before moving Bishops for Black :
select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - (s.mn+1))/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'B') as b ON a.id = b.id 
where LEFT(a.move, 1)= 'B' GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 1  group by f.id ORDER BY f.id ) n ;

-- Average moves before moving Rooks (Castles) for Black  :
select avg(num_of_moves) as avg_moves from ( 
select min((f.moveId - (s.mn+1))/2) as num_of_moves  from (
( SELECT a.id,a.moveId FROM gameMoves AS a
JOIN (select distinct(id) as id from gameMoves where LEFT(move, 1)= 'R' OR LEFT(move, 1)= 'O') as b ON a.id = b.id 
where LEFT(move, 1)= 'R' OR LEFT(move, 1)= 'O'  GROUP BY a.Id , moveId ORDER BY a.moveid)) f 
join (select id,min(moveid) as mn from gameMoves group by id) s on f.id = s.id where (f.moveId - s.mn)%2 = 1  group by f.id ORDER BY f.id ) n ;



-- Question 1
select 
(select count(*) as white_wins from gameStatus  where winner = 'white')/
(select count(*) as total_game from gameStatus) as fraction;

-- Question 2
-- when white wins
select AVG(turns) from chessGames c join gameStatus g on c.id = g.id where g.winner = 'white'; 

-- when Black wins
select AVg(turns) from chessGames c
join gameStatus g on c.id = g.id
where g.winner = 'black'; 


-- Question 3

select sum(case when length(g.move)<3 then 1 else 0  end)/count(g.move) as fraction_games_started_pwan from gameMoves as g
join (select min(moveid) as moveid from gameMoves group by id ) as c on c.moveid=g.moveid;
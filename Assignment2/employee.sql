
DROP TABLE IF EXISTS Department;           
CREATE TABLE Department(deptID INT,deptName VARCHAR(100),location VARCHAR(100));                    
          
DROP TABLE IF EXISTS Employee;  
CREATE TABLE Employee(empID INT,empName VARCHAR(100),job VARCHAR(100),deptID INT,salary INT);
                  
 DROP TABLE IF EXISTS Assigned;             
CREATE TABLE Assigned(empID INT,projID INT,role VARCHAR(100));                 
        
DROP TABLE IF EXISTS Project;           
CREATE TABLE Project(projID INT,title VARCHAR(100),budget INT,funds INT);

INSERT INTO Department(deptID, deptName, location) VALUES(3, 'marketing', '123 Fake Street, Waterloo, Ontario, N2N 2N2');             
INSERT INTO Department(deptID, deptName, location) VALUES(7, 'research', '124 Unreal Way, Guelph, Ontario, N3N 3N3');            
INSERT INTO Department(deptID, deptName, location) VALUES(12, 'software', '1 Bay Street, Toronto, Ontario, M5W 1E6'); 
INSERT INTO Department(deptID, deptName, location) VALUES(13, 'computing', '1 Bay Street, Toronto, Ontario, M5W 1E6'); 

INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(23, 'Mary Jane Smith', 'programmer', 12, 35000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(45, 'Kelly Kelly', 'engineer', 7, 37000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(56, 'Adam Andrew Herr', 'janitor', 7, 26000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(89, 'William William Williams', 'analyst', 12, 36000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(77, 'Bryan Hergot', 'secretary', 7, 28000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(66, 'Brian Hess', 'technician', 7, 32000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(92, 'Mary Mays', 'engineer', 7, 45000); 
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(68, 'Joe Morris', 'secretary', 3, 23000);  
INSERT INTO Employee(empID, empName, job, deptID, salary) VALUES(69, 'Maria Jones', 'engineer', 3, 32000); 

INSERT INTO Project(projID, title, budget, funds) VALUES(345, 'compiler', 500000, 250000); 
INSERT INTO Project(projID, title, budget, funds) VALUES(123, 'display', 650000, 370000); 

INSERT INTO Assigned(empID, projID, role) VALUES(23, 345, 'programmer');            
INSERT INTO Assigned(empID, projID, role) VALUES(66, 123, 'programmer');        
INSERT INTO Assigned(empID, projID, role) VALUES(77, 123, 'secretary');           
INSERT INTO Assigned(empID, projID, role) VALUES(45, 123, 'manager'); 
INSERT INTO Assigned(empID, projID, role) VALUES(89, 345, 'manager');     
INSERT INTO Assigned(empID, projID, role) VALUES(92, 123, 'engineer'); 

-- Splitting Department table in two table to make scheama to be in BCNF
DROP TABLE IF EXISTS Department_Names;           
CREATE TABLE Department_Names(deptID INT,deptName VARCHAR(100));      

DROP TABLE IF EXISTS Department_Locations; 
CREATE TABLE Department_Locations(deptID INT,streetNumber INT,streetName VARCHAR(100),cityName VARCHAR(100),province VARCHAR(100),postalCode VARCHAR(100));               

-- Splitting Employee table in two table to make scheama to be in BCNF
DROP TABLE IF EXISTS Employee_Details;  
CREATE TABLE Employee_Details(empID INT,firstName VARCHAR(100),middleName VARCHAR(100),lastName VARCHAR(100),job VARCHAR(100),salary INT);

DROP TABLE IF EXISTS Employee_Dept_Mapping;  
CREATE TABLE Employee_Dept_Mapping(empID INT,deptID INT);       

-- Migration of Data from existing tables to newly split tables 
INSERT INTO Department_Names (deptID,deptName) select distinct deptId,deptName from Department;

INSERT INTO Department_Locations (deptID,streetNumber,streetName,cityName,province,postalCode) 
select  distinct 
	deptId,
	substring_index(substring_index(location,',',1 ),' ',1) streetNumber,
    substr(substring_index(location,',',1 ),length(substring_index(substring_index(location,',',1 ),' ',1))+1,length(substring_index(location,',',1 ))-length(substring_index(substring_index(location,',',1 ),' ',1))) streetName,
    substr(location,length(substring_index(location,',',1 ))+2,length(substring_index(location,',',2 )) - length(substring_index(location,',',1 ))-1) region,
    substr(location,length(substring_index(location,',',2 ))+2,length(substring_index(location,',',3 )) - length(substring_index(location,',',2 ))-1) province,
    substr(location,length(substring_index(location,',',3 ))+2,length(substring_index(location,',',4 )) - length(substring_index(location,',',3 ))-1) postal_code
FROM Department;

INSERT INTO Employee_Details (empID,firstName,middleName,lastName,job,salary) 
select  distinct 
	empID,
	substring_index(empName,' ',1 ) firstName,
	substr(empName,length(SUBSTRING_INDEX(empName, " ",1))+2,length(empName)-length(SUBSTRING_INDEX(empName, " ",-2))) middleName,
        substring_index(empName, ' ', -1) lastName,
	job,
	salary
from Employee ;

INSERT INTO Employee_Dept_Mapping (empID,deptID) select  distinct empID,deptId from Employee;

DROP TABLE Employee;

DROP TABLE Department;

-- creating a view for Employee table to replicate the old Employee table using newly split tables

DROP VIEW IF EXISTS Employee;
CREATE VIEW Employee AS (
	select Employee_Details.empId, CONCAT(firstName,' ',middleName,' ',lastName) as empName, job, deptID, salary from Employee_Details
	join Employee_Dept_Mapping on Employee_Dept_Mapping.empID=Employee_Details.empId
); 

-- creating a view Department to replicate the old Department table using newly split tables

DROP VIEW IF EXISTS Department;
CREATE VIEW Department AS (
	select Department_Names.deptID, deptName, CONCAT(streetNumber,' ',streetName,',',cityName,',',province,',',postalCode) location from Department_Names
	join Department_Locations on Department_Names.deptID=Department_Locations.deptId
); 

-- Primary Key creation 
ALTER TABLE Department_Names ADD PRIMARY KEY (deptID);
ALTER TABLE Department_Locations ADD PRIMARY KEY (deptID,streetNumber,streetName,cityName,province,postalCode);
ALTER TABLE Employee_Details ADD PRIMARY KEY (empID);
ALTER TABLE Employee_Dept_Mapping ADD PRIMARY KEY (empID,deptID);
ALTER TABLE Project ADD PRIMARY KEY (projID);
ALTER TABLE Assigned ADD PRIMARY KEY (empID, projID, role);

-- Foreign Key Creation
ALTER TABLE Department_Locations ADD FOREIGN KEY (deptID) REFERENCES Department_Names(deptID);
ALTER TABLE Employee_Dept_Mapping ADD FOREIGN KEY (empID) REFERENCES Employee_Details(empID);
ALTER TABLE Assigned ADD FOREIGN KEY (empID) REFERENCES Employee_Details(empID);
ALTER TABLE Assigned ADD FOREIGN KEY (projID) REFERENCES Project(projID);


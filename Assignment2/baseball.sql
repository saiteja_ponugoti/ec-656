-- ==============================================================
-- QUESTION 3 
-- ==============================================================

-- (a) How many players have an unknown birthdate?
-- --------------------------------------------------------------
-- SQL Query

-- WITH PRIMARY KEY AND FOREIGN KEY 
SELECT COUNT(playerID) AS birthDate_unknown FROM Master WHERE birthYear = '' OR birthYear = NULL OR birthMonth ='' OR birthMonth = NULL OR birthDay = '' OR birthDay = NULL ; 

-- RESULT :
-- +--------------------+
-- | birthDate_unknown |
-- +--------------------+
-- |                449 |
-- +--------------------+
-- 1 row in set (0.01 sec)

EXPLAIN SELECT COUNT(playerID) AS birthDate_unknown FROM Master WHERE birthYear = '' OR birthYear = NULL OR birthMonth ='' OR birthMonth = NULL OR birthDay = '' OR birthDay = NULL ; 

-- +----+-------------+--------+------------+------+---------------+------+---------+------+-------+----------+-------------+
-- | id | select_type | table  | partitions | type | possible_keys | key  | key_len | ref  | rows  | filtered | Extra       |
-- +----+-------------+--------+------------+------+---------------+------+---------+------+-------+----------+-------------+
-- |  1 | SIMPLE      | Master | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 18980 |    46.86 | Using where |
-- +----+-------------+--------+------------+------+---------------+------+---------+------+-------+----------+-------------+

-- WHEN AN EXPLICIT INDEX on birthYear for Master table

ALTER TABLE Master ADD INDEX index_birthYear (birthYear);

SELECT COUNT(playerID) AS birthDate_unknown FROM Master WHERE birthYear = '' OR birthYear = NULL OR birthMonth ='' OR birthMonth = NULL OR birthDay = '' OR birthDay = NULL ; 

-- RESULT :
-- +--------------------+
-- | birthDate_unknown |
-- +--------------------+
-- |                449 |
-- +--------------------+
-- 1 row in set (0.01 sec)

ALTER TABLE Master DROP INDEX index_birthYear;

-- --------------------------------------------------------------

-- (b) How many people are in the Hall of Fame? What fraction of each category of person are in
-- the Hall Of Fame? Are more people in the Hall Of Fame alive or dead? Does this vary by
-- category?
-- ----------------------------------------------------------------

-- How many people are in the Hall of Fame? 
-- WITH PRIMARY KEY AND FOREIGN KEY 
SELECT COUNT(DISTINCT(playerID)) FROM HallOfFame;



-- RESULT:
-- +---------------------------+
-- | total_people                |
-- +---------------------------+
-- |                      1260 |
-- +---------------------------+
-- 1 row in set (0.01 sec)

EXPLAIN SELECT COUNT(DISTINCT(playerID)) FROM HallOfFame;

-- +----+-------------+------------+------------+-------+---------------+---------+---------+------+------+----------+-------------+
-- | id | select_type | table      | partitions | type  | possible_keys | key     | key_len | ref  | rows | filtered | Extra       |
-- +----+-------------+------------+------------+-------+---------------+---------+---------+------+------+----------+-------------+
-- |  1 | SIMPLE      | HallOfFame | NULL       | index | PRIMARY       | PRIMARY | 1538    | NULL | 4156 |   100.00 | Using index |
-- +----+-------------+------------+------------+-------+---------------+---------+---------+------+------+----------+-------------+
-- 1 row in set, 1 warning (0.00 sec)

-- The above query when exicuted by MySQL is already using index for resolving the query ,so creating and index is unnecessary in this case 

-- What fraction of each category of person are in the Hall Of Fame? 
-- WITH PRIMARY KEY AND FOREIGN KEY
-- SQL Query
SELECT COUNT(playerID) AS num_per_cat,category FROM (SELECT playerID,category FROM HallOfFame GROUP BY playerID,category) AS A GROUP BY A.category;

-- RESULT : 
-- +----------+-------------------+
-- | num_per_cat | category      |
-- +----------+-------------------+
-- |       23 | Manager           |
-- |       35 | Pioneer/Executive |
-- |     1208 | Player            |
-- |       10 | Umpire            |
-- +----------+-------------------+
-- 4 rows in set (0.01 sec)


EXPLAIN SELECT COUNT(playerID) AS num_per_cat,category FROM (SELECT playerID,category FROM HallOfFame GROUP BY playerID,category) AS A GROUP BY A.category;

-- +----+-------------+------------+------------+------+---------------+------+---------+------+------+----------+---------------------------------+
-- | id | select_type | table      | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra                           |
-- +----+-------------+------------+------------+------+---------------+------+---------+------+------+----------+---------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 4156 |   100.00 | Using temporary; Using filesort |
-- |  2 | DERIVED     | HallOfFame | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 4156 |   100.00 | Using temporary; Using filesort |
-- +----+-------------+------------+------------+------+---------------+------+---------+------+------+----------+---------------------------------+
-- 2 rows in set, 1 warning (0.00 sec)

-- WITH EXPLICIT INDEX ON CATEGORY FIELD
ALTER TABLE HallOfFame ADD INDEX index_category (category);

SELECT COUNT(playerID) AS num_per_cat,category FROM (SELECT playerID,category FROM HallOfFame GROUP BY playerID,category) AS A GROUP BY A.category;

-- RESULT : 
-- +----------+-------------------+
-- | num_per_cat | category      |
-- +----------+-------------------+
-- |       23 | Manager           |
-- |       35 | Pioneer/Executive |
-- |     1208 | Player            |
-- |       10 | Umpire            |
-- +----------+-------------------+
-- 4 rows in set (0.01 sec)

EXPLAIN SELECT COUNT(playerID) AS num_per_cat,category FROM (SELECT playerID,category FROM HallOfFame GROUP BY playerID,category) AS A GROUP BY A.category;

--  +----+-------------+------------+------------+-------+----------------+----------------+---------+------+------+----------+----------------------------------------------+
-- | id | select_type | table      | partitions | type  | possible_keys  | key            | key_len | ref  | rows | filtered | Extra                                        |
-- +----+-------------+------------+------------+-------+----------------+----------------+---------+------+------+----------+----------------------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL   | NULL           | NULL           | NULL    | NULL | 4156 |   100.00 | Using temporary; Using filesort              |
-- |  2 | DERIVED     | HallOfFame | NULL       | index | index_category | index_category | 768     | NULL | 4156 |   100.00 | Using index; Using temporary; Using filesort |
-- +----+-------------+------------+------------+-------+----------------+----------------+---------+------+------+----------+----------------------------------------------+
-- 2 rows in set, 1 warning (0.00 sec)

-- Not much improvement in th permformance as it remains same at 0.01 sec where as possible key changes to created index_category.

ALTER TABLE HallOfFame DROP INDEX index_category;
-- The fraction of each category of persons is : 
-- Manager = 0.018 ,  Pioneer/Executive = 0.027 , Player = 0.946 , Umpire = 0.007

-- Are more people in the Hall Of Fame alive or dead?

-- WITH PRIMARY KEY AND FOREIGN KEY
-- SQL Querey

SELECT SUM(dead)-SUM(alive) AS dead_minus_alive FROM (SELECT playerID,CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,CASE WHEN deathYear !='' THEN 1 ELSE 0 end AS dead FROM Master WHERE playerID in (SELECT DISTINCT(playerID) FROM HallOfFame)) AS res;


-- RESULT : Dead people in HallOfFame are more than the alive ones.
-- +------------------+
-- | dead_minus_alive |
-- +------------------+
-- |               47 |
-- +------------------+
-- 1 row in set (0.02 sec)

EXPLAIN SELECT SUM(dead)-SUM(alive) AS dead_minus_alive FROM (SELECT playerID,CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,CASE WHEN deathYear !='' THEN 1 ELSE 0 end AS dead FROM Master WHERE playerID in (SELECT DISTINCT(playerID) FROM HallOfFame)) AS res;

-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------+
-- | id | select_type | table      | partitions | type   | possible_keys | key     | key_len | ref                       | rows | filtered | Extra                  |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------+
-- |  1 | SIMPLE      | HallOfFame | NULL       | index  | PRIMARY       | PRIMARY | 1538    | NULL                      | 4156 |    30.32 | Using index; LooseScan |
-- |  1 | SIMPLE      | Master     | NULL       | eq_ref | PRIMARY       | PRIMARY | 767     | assg1.HallOfFame.playerID |    1 |   100.00 | NULL                   |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------+
-- 2 rows in set, 1 warning (0.00 sec)


-- Does this vary by category?

-- Managers : Dead(18), alive(5) -> dead are more
-- Pioneer/Exicutive : Dead(32), alive(3) -> dead are more
-- Player : Dead(609), alive(598) -> dead are more
-- Umpire : Dead(9), alive(1) -> dead are more

-- So for all category the result is same i.e. the dead people are more than alive ones.
--

-- Example query used for deriving above results ,buy cahnging the category in below query :

SELECT SUM(dead),SUM(alive) AS dead_minus_alive FROM 
(SELECT DISTINCT Master.playerID,
	CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,
	CASE WHEN deathYear !='' THEN 1 ELSE 0 END AS dead 
FROM Master INNER JOIN HallOfFame ON Master.playerID=HallOfFame.playerID WHERE category='Player') AS res;

-- +-----------+------------------+
-- | SUM(dead) | dead_minus_alive |
-- +-----------+------------------+
-- |       609 |              599 |
-- +-----------+------------------+
-- 1 row in set (0.01 sec)


EXPLAIN SELECT SUM(dead),SUM(alive) AS dead_minus_alive FROM 
(SELECT DISTINCT Master.playerID,
	CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,
	CASE WHEN deathYear !='' THEN 1 ELSE 0 END AS dead 
FROM Master INNER JOIN HallOfFame ON Master.playerID=HallOfFame.playerID WHERE category='Player') AS res;

-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------------+
-- | id | select_type | table      | partitions | type   | possible_keys | key     | key_len | ref                       | rows | filtered | Extra                        |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL    | NULL          | NULL    | NULL    | NULL                      |  415 |   100.00 | NULL                         |
-- |  2 | DERIVED     | HallOfFame | NULL       | ALL    | PRIMARY       | NULL    | NULL    | NULL                      | 4156 |    10.00 | Using where; Using temporary |
-- |  2 | DERIVED     | Master     | NULL       | eq_ref | PRIMARY       | PRIMARY | 767     | assg1.HallOfFame.playerID |    1 |   100.00 | NULL                         |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+---------------------------+------+----------+------------------------------+

ALTER TABLE HallOfFame ADD INDEX index_category (category);

EXPLAIN SELECT SUM(dead),SUM(alive) AS dead_minus_alive FROM 
(SELECT DISTINCT Master.playerID,
	CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,
	CASE WHEN deathYear !='' THEN 1 ELSE 0 END AS dead 
FROM Master INNER JOIN HallOfFame ON Master.playerID=HallOfFame.playerID WHERE category='Player') AS res;

-- +----+-------------+------------+------------+--------+------------------------+----------------+---------+---------------------------+------+----------+------------------------------+
-- | id | select_type | table      | partitions | type   | possible_keys          | key            | key_len | ref                       | rows | filtered | Extra                        |
-- +----+-------------+------------+------------+--------+------------------------+----------------+---------+---------------------------+------+----------+------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL    | NULL                   | NULL           | NULL    | NULL                      | 4031 |   100.00 | NULL                         |
-- |  2 | DERIVED     | HallOfFame | NULL       | ref    | PRIMARY,index_category | index_category | 768     | const                     | 4031 |   100.00 | Using index; Using temporary |
-- |  2 | DERIVED     | Master     | NULL       | eq_ref | PRIMARY                | PRIMARY        | 767     | assg1.HallOfFame.playerID |    1 |   100.00 | NULL                         |
-- +----+-------------+------------+------------+--------+------------------------+----------------+---------+---------------------------+------+----------+------------------------------+

ALTER TABLE HallOfFame DROP INDEX index_category;

-- --------------------------------------------------------------

-- (c) What are the names and total pay (individually) of the three people with the three largest total
-- salaries? What category are these people (Players? Managers? Other?)? What are the top
-- three by category?
-- ----------------------------------------------------------------

--  What are the names and total pay (individually) of the three people with the three largest total
-- salaries?

select Master.nameGiven as name,sum(salary) as total_pay from Salaries join (select distinct a.playerID from (select distinct playerId,salary from Salaries order by salary desc) as a limit 3) as req on req.playerID =Salaries.playerID  inner join Master on Master.playerID=Salaries.playerID group by Salaries.playerID,Master.nameGiven order by sum(salary) desc;

-- RESULT :
-- +--------------------+-----------+
-- | name               | total_pay |
-- +--------------------+-----------+
-- | Alexander Enmanuel | 398416252 |
-- | Donald Zachary     | 144302030 |
-- | Clayton Edward     |  90415000 |
-- +--------------------+-----------+
-- 3 rows in set (0.26 sec)


Explain select Master.nameGiven as name,sum(salary) as total_pay from Salaries join (select distinct a.playerID from (select distinct playerId,salary from Salaries order by salary desc) as a limit 3) as req on req.playerID =Salaries.playerID  inner join Master on Master.playerID=Salaries.playerID group by Salaries.playerID,Master.nameGiven order by sum(salary) desc;

-- +----+-------------+------------+------------+--------+---------------+---------+---------+--------------+-------+----------+---------------------------------+
-- | id | select_type | table      | partitions | type   | possible_keys | key     | key_len | ref          | rows  | filtered | Extra                           |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+--------------+-------+----------+---------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL    | NULL          | NULL    | NULL    | NULL         |     3 |   100.00 | Using temporary; Using filesort |
-- |  1 | PRIMARY     | Master     | NULL       | eq_ref | PRIMARY       | PRIMARY | 767     | req.playerID |     1 |   100.00 | NULL                            |
-- |  1 | PRIMARY     | Salaries   | NULL       | ref    | PRIMARY       | PRIMARY | 767     | req.playerID |     5 |   100.00 | NULL                            |
-- |  2 | DERIVED     | <derived3> | NULL       | ALL    | NULL          | NULL    | NULL    | NULL         | 26648 |   100.00 | Using temporary                 |
-- |  3 | DERIVED     | Salaries   | NULL       | ALL    | NULL          | NULL    | NULL    | NULL         | 26648 |   100.00 | Using temporary; Using filesort |
-- +----+-------------+------------+------------+--------+---------------+---------+---------+--------------+-------+----------+---------------------------------+
-- 5 rows in set, 

ALTER TABLE Salaries ADD INDEX index_salary (salary);

-- +----+-------------+------------+------------+--------+---------------+--------------+---------+--------------+-------+----------+---------------------------------+
-- | id | select_type | table      | partitions | type   | possible_keys | key          | key_len | ref          | rows  | filtered | Extra                           |
-- +----+-------------+------------+------------+--------+---------------+--------------+---------+--------------+-------+----------+---------------------------------+
-- |  1 | PRIMARY     | <derived2> | NULL       | ALL    | NULL          | NULL         | NULL    | NULL         |     3 |   100.00 | Using temporary; Using filesort |
-- |  1 | PRIMARY     | Master     | NULL       | eq_ref | PRIMARY       | PRIMARY      | 767     | req.playerID |     1 |   100.00 | NULL                            |
-- |  1 | PRIMARY     | Salaries   | NULL       | ref    | PRIMARY       | PRIMARY      | 767     | req.playerID |     5 |   100.00 | NULL                            |
-- |  2 | DERIVED     | <derived3> | NULL       | ALL    | NULL          | NULL         | NULL    | NULL         | 26648 |   100.00 | Using temporary                 |
-- |  3 | DERIVED     | Salaries   | NULL       | index  | index_salary  | index_salary | 5       | NULL         | 26648 |   100.00 | Using index; Using filesort     |
-- +----+-------------+------------+------------+--------+---------------+--------------+---------+--------------+-------+----------+---------------------------------+

ALTER TABLE Salaries DROP INDEX index_salary;

-- (d)What is the average number of Home Runs a player hAS?
-- -----------------------------------------------------------------
-- SQL Query :

SELECT AVG(HR) FROM Batting;

-- RESULT :
-- +---------+
-- | AVG(HR) |
-- +---------+
-- |  2.8136 |
-- +---------+
-- 1 row in set (0.04 sec)

EXPLAIN SELECT AVG(HR) FROM Batting;

-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------+
-- | id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows   | filtered | Extra |
-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------+
-- |  1 | SIMPLE      | Batting | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 102386 |   100.00 | NULL  |
-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------+

-- ADDING EPLICIT INDEX ON HR COLUMN FOR BATTING TABLE
ALTER TABLE Batting ADD INDEX index_HR(HR);

SELECT AVG(HR) FROM Batting;

-- +---------+
-- | AVG(HR) |
-- +---------+
-- |  2.8136 |
-- +---------+
-- 1 row in set (0.02 sec)

EXPLAIN SELECT AVG(HR) FROM Batting;
-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+--------+----------+-------------+
-- | id | select_type | table   | partitions | type  | possible_keys | key      | key_len | ref  | rows   | filtered | Extra       |
-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+--------+----------+-------------+
-- |  1 | SIMPLE      | Batting | NULL       | index | NULL          | index_HR | 5       | NULL | 102386 |   100.00 | Using index |
-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+--------+----------+-------------+

ALTER TABLE Batting DROP INDEX index_HR;
-- (e) If we only COUNT players who got at leASt 1 Home Run, what is the average number of Home
-- Runs a player hAS?
-- -------------------------------------------------------------------

-- Relational Algebra:
-- π avg_HR_grtthan_0(γ;COUNT(playerID)→avg_HR_grtthan_0( σHR>0 (Batting))) ;

-- SQL Query

SELECT avg(HR) AS avg_HR_grtthan_0 FROM Batting WHERE HR>0;

-- RESULT :

-- +------------------+
-- | avg_HR_grtthan_0 |
-- +------------------+
-- |           7.2428 |
-- +------------------+
-- 1 row in set (0.03 sec)

EXPLAIN SELECT avg(HR) AS avg_HR_grtthan_0 FROM Batting WHERE HR>0;

-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------------+
-- | id | select_type | table   | partitions | type | possible_keys | key  | key_len | ref  | rows   | filtered | Extra       |
-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------------+
-- |  1 | SIMPLE      | Batting | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 102386 |    33.33 | Using where |
-- +----+-------------+---------+------------+------+---------------+------+---------+------+--------+----------+-------------+
-- 1 row in set, 1 warning (0.00 sec)

-- ADDING EPLICIT INDEX ON HR COLUMN FOR BATTING TABLE
ALTER TABLE Batting ADD INDEX index_HR(HR);

SELECT avg(HR) AS avg_HR_grtthan_0 FROM Batting WHERE HR>0;

-- +------------------+
-- | avg_HR_grtthan_0 |
-- +------------------+
-- |           7.2428 |
-- +------------------+
-- 1 row in set (0.01 sec)

EXPLAIN SELECT avg(HR) AS avg_HR_grtthan_0 FROM Batting WHERE HR>0;

-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+-------+----------+--------------------------+
-- | id | select_type | table   | partitions | type  | possible_keys | key      | key_len | ref  | rows  | filtered | Extra                    |
-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+-------+----------+--------------------------+
-- |  1 | SIMPLE      | Batting | NULL       | range | index_HR      | index_HR | 5       | NULL | 51193 |   100.00 | Using where; Using index |
-- +----+-------------+---------+------------+-------+---------------+----------+---------+------+-------+----------+--------------------------+
-- 1 row in set, 1 warning (0.01 sec)

ALTER TABLE Batting DROP INDEX index_HR;
-- -------------------------------------------------------------------
-- (f)If we define a player AS a good batter if they have more than the average number of Home Runs,
-- and a player is a good Pitcher if they have more than the average number of ShutOut -- --games, THEN      -- how many players are both good batters and good pitchers?
-- ------------------------------------------------------------------- 

-- SQL Query 

SELECT COUNT(batter.playerID) AS both_gb_gp FROM (SELECT playerID, AVG(HR) FROM Batting GROUP BY playerID  HAVING AVG(HR) > (SELECT AVG(HR) FROM Batting)) AS batter INNER JOIN (SELECT playerID, AVG(SHO) FROM Pitching  GROUP BY playerID HAVING AVG(SHO) > (SELECT AVG(SHO) FROM Pitching)) AS pitcher ON batter.playerID=pitcher.playerID ;


-- RESULT :

-- +------------+
-- | both_gb_gp |
-- +------------+
-- |          7 |
-- +------------+
-- 1 row in set (0.18 sec)

EXPLAIN SELECT COUNT(batter.playerID) AS both_gb_gp FROM (SELECT playerID, AVG(HR) FROM Batting GROUP BY playerID  HAVING AVG(HR) > (SELECT AVG(HR) FROM Batting)) AS batter INNER JOIN (SELECT playerID, AVG(SHO) FROM Pitching  GROUP BY playerID HAVING AVG(SHO) > (SELECT AVG(SHO) FROM Pitching)) AS pitcher ON batter.playerID=pitcher.playerID ;

-- +----+-------------+------------+------------+-------+----------------+-------------+---------+------------------+--------+----------+-------+
-- | id | select_type | table      | partitions | type  | possible_keys  | key         | key_len | ref              | rows   | filtered | Extra |
-- +----+-------------+------------+------------+-------+----------------+-------------+---------+------------------+--------+----------+-------+
-- |  1 | PRIMARY     | <derived4> | NULL       | ALL   | NULL           | NULL        | NULL    | NULL             |  44629 |   100.00 | NULL  |
-- |  1 | PRIMARY     | <derived2> | NULL       | ref   | <auto_key0>    | <auto_key0> | 767     | pitcher.playerID |     10 |   100.00 | NULL  |
-- |  4 | DERIVED     | Pitching   | NULL       | index | PRIMARY,teamID | PRIMARY     | 775     | NULL             |  44629 |   100.00 | NULL  |
-- |  5 | SUBQUERY    | Pitching   | NULL       | ALL   | NULL           | NULL        | NULL    | NULL             |  44629 |   100.00 | NULL  |
-- |  2 | DERIVED     | Batting    | NULL       | index | PRIMARY,teamID | PRIMARY     | 775     | NULL             | 102386 |   100.00 | NULL  |
-- |  3 | SUBQUERY    | Batting    | NULL       | ALL   | NULL           | NULL        | NULL    | NULL             | 102386 |   100.00 | NULL  |
-- +----+-------------+------------+------------+-------+----------------+-------------+---------+------------------+--------+----------+-------+
-- 6 rows in set, 1 warning (0.00 sec)

-- ADDING EXPLICIT INDEX FOR BATTING AND PITCHING TABLES;
ALTER TABLE Batting ADD INDEX index_HR (HR);
ALTER TABLE Pitching ADD INDEX index_SHO (SHO);

SELECT COUNT(batter.playerID) AS both_gb_gp FROM (SELECT playerID, AVG(HR) FROM Batting GROUP BY playerID  HAVING AVG(HR) > (SELECT AVG(HR) FROM Batting)) AS batter INNER JOIN (SELECT playerID, AVG(SHO) FROM Pitching  GROUP BY playerID HAVING AVG(SHO) > (SELECT AVG(SHO) FROM Pitching)) AS pitcher ON batter.playerID=pitcher.playerID ;
-- +------------+
-- | both_gb_gp |
-- +------------+
-- |          7 |
-- +------------+
-- 1 row in set (0.11 sec)

EXPLAIN SELECT COUNT(batter.playerID) AS both_gb_gp FROM (SELECT playerID, AVG(HR) FROM Batting GROUP BY playerID  HAVING AVG(HR) > (SELECT AVG(HR) FROM Batting)) AS batter INNER JOIN (SELECT playerID, AVG(SHO) FROM Pitching  GROUP BY playerID HAVING AVG(SHO) > (SELECT AVG(SHO) FROM Pitching)) AS pitcher ON batter.playerID=pitcher.playerID ;

-- +----+-------------+------------+------------+-------+--------------------------+-------------+---------+------------------+--------+----------+-------------+
-- | id | select_type | table      | partitions | type  | possible_keys            | key         | key_len | ref              | rows   | filtered | Extra       |
-- +----+-------------+------------+------------+-------+--------------------------+-------------+---------+------------------+--------+----------+-------------+
-- |  1 | PRIMARY     | <derived4> | NULL       | ALL   | NULL                     | NULL        | NULL    | NULL             |  44629 |   100.00 | NULL        |
-- |  1 | PRIMARY     | <derived2> | NULL       | ref   | <auto_key0>              | <auto_key0> | 767     | pitcher.playerID |     10 |   100.00 | NULL        |
-- |  4 | DERIVED     | Pitching   | NULL       | index | PRIMARY,teamID,index_SHO | PRIMARY     | 775     | NULL             |  44629 |   100.00 | NULL        |
-- |  5 | SUBQUERY    | Pitching   | NULL       | index | NULL                     | index_SHO   | 5       | NULL             |  44629 |   100.00 | Using index |
-- |  2 | DERIVED     | Batting    | NULL       | index | PRIMARY,teamID,index_HR  | PRIMARY     | 775     | NULL             | 102386 |   100.00 | NULL        |
-- |  3 | SUBQUERY    | Batting    | NULL       | index | NULL                     | index_HR    | 5       | NULL             | 102386 |   100.00 | Using index |
-- +----+-------------+------------+------------+-------+--------------------------+-------------+---------+------------------+--------+----------+-------------+
-- 6 rows in set, 1 warning (0.00 sec)

ALTER TABLE Batting DROP INDEX index_HR;
ALTER TABLE Pitching DROP INDEX index_SHO;


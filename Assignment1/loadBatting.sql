-- ==============================================================
-- Name : SAI TEJA PONUGOTI
-- username : stponugo
-- studentID : 20841154
-- ==============================================================
-- QUESTION 2 
-- ==============================================================

-- The load statement is working without any warnings and the data has been loaded successfully.

-- Where is the CSV data located relative to the CLI and to the DB Server?
-- The CSV File is located in the same system ,where the CLI and DB server reside.The CSV file is loacated in the file directory : "/home/sai/Desktop/Winter 2020/EC 656/Assignments/Assignment1" of local system

--  How long it takes to LOAD the CSV vs. Using the equivalent INSERT statement method.
--  Time taken to load using CSV file using CLI :

-- SQL Query for Loading CSV Data:

LOAD DATA LOCAL INFILE 'Batting.csv' REPLACE INTO TABLE Batting 
FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS 
(playerID,yearID,stint,teamID,lgID,G,AB,R,H,2B,3B,HR,@nRBI,@nSB,@nCS,BB,@nSO,IBB,HBP,SH,SF,GIDP) 
SET SB = if(@nSB='',0,@nSB), 
CS = if(@nCS='',0,@nCS), 
RBI = if(@nRBI='',0,@nRBI),
SO = if(@nSO='',0,@nSO);

-- RESULT :

-- Query OK, 105861 rows affected (7.74 sec)
-- Records: 105861  Deleted: 0  Skipped: 0  Warnings: 0

-- Time taken to exicute equvivaent insert statment by running script using CLI :

SOURCE lahman2016-batting.sql ;

-- RESULT : 

-- Query OK, 0 rows affected (0.00 sec) -- BEGIN Statement

-- Query OK, 102816 rows affected (13.32 sec)
-- Records: 102816  Duplicates: 0  Warnings: 0

-- Query OK, 0 rows affected (0.41 sec) – COMMIT statement


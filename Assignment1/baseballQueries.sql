-- ==============================================================
-- QUESTION 3 
-- ==============================================================

-- (a) How many players have an unknown birthdate?
-- --------------------------------------------------------------

-- SQL Query
SELECT COUNT(playerID) AS birthDate_unknown FROM Master 
WHERE birthYear = '' OR birthYear = NULL OR birthMonth ='' OR birthMonth = NULL OR birthDay = '' OR birthDay = NULL ; 

-- RESULT :
-- +--------------------+
-- | birthDate_unknown |
-- +--------------------+
-- |                449 |
-- +--------------------+
-- 1 row in set (0.01 sec)

-- --------------------------------------------------------------

-- (b) How many people are in the Hall of Fame? What fraction of each category of person are in
-- the Hall Of Fame? Are more people in the Hall Of Fame alive or dead? Does this vary by
-- category?
-- ----------------------------------------------------------------

-- How many people are in the Hall of Fame?

-- SQL Query

-- How many people are in the Hall of Fame? 
SELECT COUNT(DISTINCT(playerID)) FROM HallOfFame;

-- RESULT:
-- +---------------------------+
-- | total_people                |
-- +---------------------------+
-- |                      1260 |
-- +---------------------------+
-- 1 row in set (0.05 sec)


-- What fraction of each category of person are in the Hall Of Fame? 

-- SQL Query
SELECT COUNT(playerID) AS num_per_cat,category FROM (SELECT playerID,category FROM HallOfFame GROUP BY playerID,category) AS A GROUP BY A.category;

-- RESULT : 
-- +----------+-------------------+
-- | num_per_cat | category      |
-- +----------+-------------------+
-- |       23 | Manager           |
-- |       35 | Pioneer/Executive |
-- |     1208 | Player            |
-- |       10 | Umpire            |
-- +----------+-------------------+
-- 4 rows in set (0.01 sec)

-- The fraction of each category of persons is : 
-- Manager = 0.018 ,  Pioneer/Executive = 0.027 , Player = 0.946 , Umpire = 0.007

-- Are more people in the Hall Of Fame alive or dead?

-- SQL Querey

SELECT SUM(dead)-SUM(alive) AS dead_minus_alive FROM (SELECT playerID,CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,CASE WHEN deathYear !='' THEN 1 ELSE 0 end AS dead FROM Master WHERE playerID in (SELECT DISTINCT(playerID) FROM HallOfFame)) AS res;

-- RESULT : Dead people in HallOfFame are more than the alive ones.
-- +------------------+
-- | dead_minus_alive |
-- +------------------+
-- |               47 |
-- +------------------+
-- 1 row in set (0.01 sec)

-- Does this vary by category?

-- Managers : Dead(18), alive(5) -> dead are more
-- Pioneer/Exicutive : Dead(32), alive(3) -> dead are more
-- Player : Dead(609), alive(598) -> dead are more
-- Umpire : Dead(9), alive(1) -> dead are more

-- So for all category the result is same i.e. the dead people are more than alive ones.

-- Example query used for deriving above results ,buy changing the category in below query :

SELECT SUM(dead),SUM(alive) AS dead_minus_alive FROM 
(SELECT DISTINCT Master.playerID,
	CASE WHEN deathYear='' THEN 1 ELSE 0 end AS alive,
	CASE WHEN deathYear !='' THEN 1 ELSE 0 END AS dead 
FROM Master INNER JOIN HallOfFame ON Master.playerID=HallOfFame.playerID WHERE category='Player') AS res;

-- --------------------------------------------------------------

-- (c) What are the names and total pay (individually) of the three people with the three largest total
-- salaries? What category are these people (Players? Managers? Other?)? What are the top
-- three by category?
-- ----------------------------------------------------------------

--  What are the names and total pay (individually) of the three people with the three largest total
-- salaries?

SELECT Master.nameGiven AS name,SUM(salary) AS total_pay FROM Salaries 
JOIN (SELECT DISTINCT a.playerID FROM (SELECT DISTINCT playerId,salary FROM Salaries ORDER BY salary DESC)
AS a LIMIT 3) AS req ON req.playerID =Salaries.playerID  
INNER JOIN Master ON Master.playerID=Salaries.playerID 
GROUP BY Salaries.playerID ORDER BY SUM(salary) DESC;

-- RESULT :
-- +--------------------+-----------+
-- | name               | total_pay |
-- +--------------------+-----------+
-- | Alexander Enmanuel | 398416252 |
-- | Donald Zachary     | 144302030 |
-- | Clayton Edward     |  90415000 |
-- +--------------------+-----------+
-- 3 rows in set (0.20 sec)


-- (d)What is the average number of Home Runs a player hAS?
-- -----------------------------------------------------------------

-- SQL Query :

SELECT AVG(HR) FROM Batting;


-- RESULT :
-- +---------+
-- | AVG(HR) |
-- +---------+
-- |  2.8136 |
-- +---------+
-- 1 row in set (0.46 sec)

-- (e) If we only COUNT players who got at leASt 1 Home Run, what is the average number of Home
-- Runs a player hAS?
-- -------------------------------------------------------------------

-- SQL Query

SELECT AVG(HR) AS avg_HR_grtthan_0 FROM Batting WHERE HR>0;

-- RESULT :

-- +------------------+
-- | avg_HR_grtthan_0 |
-- +------------------+
-- |           7.2428 |
-- +------------------+
-- 1 row in set (0.03 sec)

-- -------------------------------------------------------------------
-- (f)If we define a player AS a good batter if they have more than the average number of Home Runs,
-- and a player is a good Pitcher if they have more than the average number of ShutOut -- --games, THEN      -- how many players are both good batters and good pitchers?
-- ------------------------------------------------------------------- 

-- SQL Query 

SELECT COUNT(batter.playerID) AS both_gb_gp FROM (SELECT playerID, AVG(HR) FROM Batting GROUP BY playerID  HAVING AVG(HR) > (SELECT AVG(HR) FROM Batting)) AS batter INNER JOIN (SELECT playerID, AVG(SHO) FROM Pitching  GROUP BY playerID HAVING AVG(SHO) > (SELECT AVG(SHO) FROM Pitching)) AS pitcher ON batter.playerID=pitcher.playerID

-- RESULT :

-- +------------+
-- | both_gb_gp |
-- +------------+
-- |          7 |
-- +------------+
-- 1 row in set (0.35 sec)
